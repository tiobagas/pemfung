-- 1. Define kpk function
kpk a b = [z | z <- map (*a) [1 .. ], z `mod` b == 0] !! 0

-- 1.1 Define fpb
fpb a b = last [z | z <- [1 .. (max a b)], a `mod` z == 0, b `mod` z == 0]

-- 2. Eval list comprehension
-- ANSWER: Memasangkan tiap elemen dari list [1 .. 3] dengan tiap
--         elemen dari list [1 .. (2*x)]
--         HASIL: [(1,1),(1,2),(2,1),(2,2),(2,3),(2,4),
--                 (3,1),(3,2),(3,3),(3,4),(3,5),(3,6)]
lstPair = [(x,y) | x <- [1 .. 3], y <- [1 .. (2*x)]]

-- 3. Define mergeSort with length, take n, and drop n
merge [] kanan = kanan
merge kiri []  = kiri
merge kiri@(x:xs) kanan@(y:ys) | x < y  = merge xs kanan
                               | y <= x = merge kiri ys

mergeSort xs = merge (mergeSort kiri) (mergeSort kanan)
    where
        kiri  = take ((length xs) `div` 2) xs
        kanan = drop ((length xs) `div` 2) xs

-- 4. Define maxList
max' x y | x > y  = x                  -- Evaluation function
        | y >= x = y
maxList [] = 0                        -- Base Case
maxList (x:xs) = max x (maxList xs)   -- Recursive Case

-- ANSWER: with foldr use eval function as first param
--         base case as second param, list as last param
maxList2 xs = foldr max' 0 xs

-- 5. Evaluasi fungsi
-- ANSWER: Fungsi akan memasangkan menjadi pair tiap elemen pada list xs
--         dengan tiap elemen pada list ys. Hasil pair akan di masukkan
--         ke dalam list baru menggunakan concat.
misteri xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)

-- 6. Phytagoras
phy = [(x,y,z) | z <- [5 .. ],
                 y <- [z, z-1 .. 1],
                 x <- [y, y-1 .. 1],
                 x*x + y*y == z*z]

-- 7. flip
flip' :: (t1 -> t2 -> t3) -> t2 -> t1 -> t3
flip' f a b = f b a

-- 10. data Direction = North | East | South | West
--              deriving (Eq, Show, Enum)
--     fromEnum: mengembalikan posisi dari objek/argumen pada enumerasi
--         ex: fromEnum North akan mengembalikan posisi dari North yaitu 0
--     toEnum: mengembalikan objek/item yang ada di posisi argumen pada enumarasi
--         ex: toEnum 0 akan mengembalikan item dari posisi ke 0 pada enumerasi
--             yaitu North

-- 8. Define fibonacci
fibbonaci = 1 : 1 : zipWith (+) fibbonaci (tail fibbonaci)