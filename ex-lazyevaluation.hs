-- 1. Evaluasi list comprehension
lstcompre = [x+y | x <- [1 .. 4], y <- [2 .. 4], x>y]

-- 2. Define divisor function
faktor a b = (a `mod` b == 0)
divisor a = [b | b <- [1 .. a], faktor a b]

-- 3. Define quicksort
quicksort [] = []
quicksort (x:xs) = quicksort [y | y <- xs, y<=x]
                   ++ [x] ++
                   quicksort [z | z <- xs, z>x]

-- 4. Infinite permutation of list
perm [] = [[]]
perm xs = [x:ps | x <- xs, ps <- perm (xs\\[x])]

-- 6. Phytagoras
phytagoras = [(x,y,z) |  z <- [5 ..]
                        , y <- [z, z-1 .. 1]
                        , x <- [y, y-1 .. 1]
                        , x*x + y*y == z*z ]