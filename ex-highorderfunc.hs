-- 1.  Exercises from chapter 9-10 of The Craft of Functional Programming
-- ######################################################################

-- Define the length function using map and sum.
changeToOne x = 1
len xs = sum (map changeToOne xs)

-- What does map (+1) (map (+1) xs) do? Can you conclude anything in general
-- about properties of map f (map g xs), where f and g are arbitrary functions?
-- ANSWER: bagian kode di atas akan mengeksekusi map g terlebih dahulu kemudian
--         mengeksekusi map f. Sebenarnya hal ini dapat disederhanakan dengan 
--         menggunakan function composition. Haskell telah memberika library
--         untuk function composition yaitu dengan . (dot).
compos f g xs = map f (map g xs)
compos2 f g xs = map (f . g) xs

-- Define function iter so that:
-- iter n f x = f (f (... (f x)))
iter 0 f x = x
iter n f x = f (iter (n-1) f x)

-- What is the type and effect of the following function?
-- \n -> iter n succ
suck n = n+1
anon = (\n -> iter n suck)

-- How would you define the sum of the squares of the natural numbers 1 to n
-- using map and foldr?
square x = x * x
sumSquare xs = foldr (+) 0 (map square xs)

-- How does the function behave?
-- ANSWER: Fungsi akan memisah tiap elemen pada list, dimana elemen2 tersebut
--         akan dimasukkan ke dalam list tersendiri. Kemudian tiap list hasil
--         dari pemisahan elemen tadi, akan di concat dengan list kosong.
mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]

-- Define a function composeList
composeList a [] = a
composeList a (x:xs) = x (composeList a xs)

-- Define a function flip
flip' f a b = f b a

-- 2.  List Comprehensions and Higher-Order Functions
-- ##################################################
-- Can you rewrite the following list comprehensions using the higher-order
-- functionsmapandfilter? You might need the function concat too.

-- [ x+1 | x <-xs ]
nomor1 xs = map (+1) xs

-- [ x+y | x <-xs, y <-ys ]
-- NOTE: Saya tidak tahu bagaimana caranya nested loop menggunakan map. 
--       Saya mencari cara lain yaitu dengan menggunakan zipWith.
nomor2 xs ys = map (\x -> map (\y -> x+y) ys) xs

-- [ x+2 | x <-xs, x > 3 ]
nomor3 xs = map (+2) (filter (>3) xs)

-- [ x+3 | (x,_) <-xys ]
-- ANSWER: fst untuk mengambil elem pertama dari tuple.
--         snd untuk mengambil elem kedua dari tuple.
nomor4 xys = map (+3) (map fst xys)

-- [ x+4 | (x,y) <-xys, x+y < 5 ]
-- nomor5 xys = 

-- [ x+5 | Just x <-mxs ]
-- nomor6 mxs =

-- Can you it the other way around? I.e. rewrite the following expressions as
-- list comprehensions.

-- map (+3) xs
nomor7 xs = [x+3 | x <- xs]

-- filter (>7) xs
nomor8 xs = [x | x <- xs, x>7]

-- concat (map (\x -> map (\y -> (x,y)) ys) xs)
nomor9 xs ys = [(x,y) | x<-xs, y<-ys]

-- filter (>3) (map (\(x,y) -> x+y) xys)
nomor10 xys = [x+y | (x,y)<-xys, x+y>3]
