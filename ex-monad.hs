-- List compre untuk membuat list of pair pada tiap
-- elemen di list a dan list b
test0 = [(x,y) | x <- [1,2,3], y <- [4,5]]

-- Monadic test0 beautify
test1 = do
    x <- [1,2,3]
    y <- [4,5]
    return (x,y)

-- Monadic test0 with bind
test2 = ([1,2,3] >>= \x -> [4,5] >>= \y -> return (x,y))