data Expr = C Float | Expr :+ Expr | Expr :- Expr
          | Expr :* Expr | Expr :/ Expr
          | V [Char]
          | Let String Expr Expr
        deriving Show

-- >>> (C 10.0 :+ (C 8 :/ C 2)) :* (C 7 :- C 4)
-- (C 10.0 :+ (C 8.0 :/ C 2.0)) :* (C 7.0 :- C 4.0)
-- Contoh di atas memperlihatkan bahwa ekspresi (C x) akan
-- mengubah x menjadi float

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1)         = if (v0 == v1) then e0 else (V v1)
subst v0 e0 (C c)          = (C c)
subst v0 e0 (e1 :+ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :* e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :/ e2)     = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)

data Direction = North | East | South | West
     deriving (Eq,Show,Enum)

right,left :: Direction -> Direction

right d = toEnum (succ (fromEnum d) `mod` 4)
left  d = toEnum (pred (fromEnum d) `mod` 4)

turnLeft  :: Robot ()
turnLeft = updateState (\s -> s {facing = left (facing s)})

turnRight :: Robot ()
turnRight = updateState (\s -> s {facing = right (facing s)})
